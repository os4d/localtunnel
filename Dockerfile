FROM alpine:3.6

ENV BUILD_DEPS="gettext"  \
    RUNTIME_DEPS="libintl"

RUN set -x && \
    apk add --update $RUNTIME_DEPS && \
    apk add --virtual build_deps $BUILD_DEPS

FROM defunctzombie/localtunnel-server

ARG BUILD_DATE
ARG VERSION
ARG LANDING=https://www.os4d.org/

RUN apk update \
    && apk add  --no-cache  \
          nginx \
          gettext \
          bash


RUN addgroup -S www && adduser -S www -G www \
    && echo $VERSION > /VERSION \
    && echo "version: $VERSION - date: $BUILD_DATE" > /RELEASE \
    && sed -i "s;https://localtunnel.github.io/www/;$LANDING;" /app/server.js

COPY --from=0 /usr/bin/envsubst /usr/local/bin/envsubst

ENV PATH="${PATH}:/root/.local/bin:/usr/local/bin/" \
    VERSION=${VERSION} \
    BUILD_DATE=${BUILD_DATE} \
    NGINX_CACHE_DIR="/data/nginx/cache" \
    NGINX_MAX_BODY_SIZE=30M \
    AUTH_KEY="" \
    AUTH_TOKEN="" \
    SERVER_NAME=""

ADD ./conf/nginx /conf/nginx
ADD ./bin/* /usr/local/bin/


ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["start"]
EXPOSE 80

