#!/bin/bash

set -e
export NGINX_MAX_BODY_SIZE="${NGINX_MAX_BODY_SIZE:-30M}"
export NGINX_CACHE_DIR="${NGINX_CACHE_DIR:-/data/nginx/cache}"
export DOLLAR='$'

mkdir -p /var/run /run/nginx /var/www/html

chown www:www -R /var/www/html


conf(){
  envsubst < /conf/nginx/nginx.conf.tpl > /conf/nginx/nginx.conf && nginx -tc /conf/nginx/nginx.conf
}

case "$1" in
    conf )
      conf
      ;;
    start )
        conf
        echo "Starting localtunnel ${VERSION} on ${LT_DOMAIN}"
        nginx -c /conf/nginx/nginx.conf
        exec node -r esm ./bin/server --port 9000 --domain ${LT_DOMAIN} --secure
    ;;
    *)
    exec $@
    ;;
esac
