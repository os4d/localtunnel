#!/bin/bash

mkdir -p /etc/letsencrypt

docker run -it --rm --name certbot \
        -v "/etc/letsencrypt:/etc/letsencrypt" \
        -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
        -e CERTBOT_DOMAIN= \
        os4d/certbot \
        get --dry-run

