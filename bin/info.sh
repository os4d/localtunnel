#!/bin/sh

echo "
- OS4D localtunnel server '${VERSION}' build '${BUILD_DATE}'
"
envsubst --version | grep 'envsubst'
node -r esm ./bin/server --help
nginx -v

echo ""
