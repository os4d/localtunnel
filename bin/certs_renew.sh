#!/bin/bash
PID=`cat /var/run/nginx.pid`

sudo docker run -it --rm --name certbot \
          -v "/etc/letsencrypt:/etc/letsencrypt" \
          -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
          os4d/certbot renew \
          --post-hook "kill -HUP $PID"