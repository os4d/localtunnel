user  www;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  10000;
}



http {
    include    /conf/nginx/mime.types;
    default_type  application/octet-stream;
    access_log  off;
    sendfile        on;
    keepalive_timeout  30;
    gzip  on;
    gzip_min_length  1000;

    add_header X-LT-Version "${VERSION}";
    add_header X-LT-Build "${BUILD_DATE}";
    add_header X-LT-Time "${DOLLAR}date_gmt";

    ssl_certificate        /etc/letsencrypt/live/${LT_DOMAIN}/fullchain.pem; # managed by Certbot
    ssl_certificate_key    /etc/letsencrypt/live/${LT_DOMAIN}/privkey.pem; # managed by Certbot

    proxy_http_version 1.1;

    # http://nginx.org/en/docs/http/websocket.html
    map ${DOLLAR}http_upgrade ${DOLLAR}connection_upgrade {
        default upgrade;
        ''      close;
    }

    upstream server {
        server 127.0.0.1:9000;
    }
    server {
        server_name  *.${LT_DOMAIN};
        listen       80;
        listen       443 ssl;

        location / {
            proxy_pass http://server/;

            proxy_set_header X-Real-IP ${DOLLAR}remote_addr;
            proxy_set_header X-Forwarded-For ${DOLLAR}proxy_add_x_forwarded_for;
            proxy_set_header Host ${DOLLAR}http_host;
            proxy_set_header X-Forwarded-Proto http;
            proxy_set_header X-NginX-Proxy true;
            proxy_set_header Upgrade ${DOLLAR}http_upgrade;
            proxy_set_header Connection ${DOLLAR}connection_upgrade;

            add_header X-Cache-Status ${DOLLAR}upstream_cache_status;
            add_header X-LT-Version "${VERSION}";
            add_header X-LT-Build "${BUILD_DATE}";
            add_header X-LT-Time "${DOLLAR}date_gmt";

            proxy_redirect off;
        }
    }

    server {
        server_name ${LT_DOMAIN};
        listen      80;
        listen      443 ssl;

        location / {
            auth_basic "Administrator’s Area";
            auth_basic_user_file /conf/nginx/.htpasswd;

            proxy_pass http://server/;

            proxy_set_header X-Real-IP ${DOLLAR}remote_addr;
            proxy_set_header X-Forwarded-For ${DOLLAR}proxy_add_x_forwarded_for;
            proxy_set_header Host ${DOLLAR}http_host;
            proxy_set_header X-Forwarded-Proto http;
            proxy_set_header X-NginX-Proxy true;
            proxy_set_header Upgrade ${DOLLAR}http_upgrade;
            proxy_set_header Connection ${DOLLAR}connection_upgrade;

            add_header X-Cache-Status ${DOLLAR}upstream_cache_status;
            add_header X-LT-Version "${VERSION}";
            add_header X-LT-Build "${BUILD_DATE}";
            add_header X-LT-Time "${DOLLAR}date_gmt";

            proxy_redirect off;
        }
    }
}
