VERSION?=dev
BUILD_DATE:=$(shell date +"%Y-%m-%d %H:%M")
CMD?=start
EXTRA?=""

.PHONY: help runlocal i18n

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z0-9_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

build:  ## build
	@echo "Building ${CI_REGISTRY_IMAGE}:${VERSION}"
	-@docker pull ${CI_REGISTRY_IMAGE}:latest

	@docker build \
		--cache-from ${CI_REGISTRY_IMAGE}:latest \
		--build-arg VERSION=${VERSION} \
		--build-arg BUILD_DATE="${BUILD_DATE}" \
		-t ${CI_REGISTRY_IMAGE}:${VERSION} \
		-f Dockerfile .
	@#docker-squash -c localtunnel.work -t ${CI_REGISTRY_IMAGE}:${VERSION}
	echo Build ${CI_REGISTRY_IMAGE}:${VERSION}
	#@docker images | grep ${CI_REGISTRY_IMAGE}
	#@docker run --rm -it -t ${CI_REGISTRY_IMAGE}:${VERSION} info.sh


info:  # retrieve image info
	 docker run --rm --tty -t ${CI_REGISTRY_IMAGE}:${VERSION} info.sh

release:
	docker tag ${CI_REGISTRY_IMAGE}:${VERSION} ${CI_REGISTRY_IMAGE}:latest
	docker push ${CI_REGISTRY_IMAGE}:${VERSION}


run:
	docker run --rm -it \
		${EXTRA} \
		-e SERVER_NAME=lt.sosbob.com \
  	    -e AUTH_KEY="${AUTH_KEY}" \
	    -e AUTH_TOKEN="${AUTH_TOKEN}" \
  	    -e DADDY_AUTH_KEY="${DADDY_AUTH_KEY}" \
	    -e DADDY_AUTH_SECRET="${DADDY_AUTH_SECRET}" \
	    -e DADDY_SERVER="${DADDY_SERVER}" \
		-p "8000:80" \
		-t ${CI_REGISTRY_IMAGE}:${VERSION} \
		${CMD}

dev:  ## dev
	CMD="/bin/bash" \
    EXTRA="-v ${PWD}/tests:/tests \
    -v ${PWD}/bin/entrypoint.sh:/usr/local/bin/entrypoint.sh \
    -v ${PWD}/bin/get_certificates.sh:/usr/local/bin/get_certificates.sh \
    -v ${PWD}/bin/info.sh:/usr/local/bin/info.sh \
	-v ${PWD}/conf/nginx:/conf/nginx \
    -v ${PWD}/~work/letsencrypt:/etc/letsencrypt" \
 	$(MAKE) run


shell:  ## shell
	EXTRA="" \
	CMD="/bin/bash" \
 	$(MAKE) run

test:
	@CMD="/tests/run_tests.sh" \
	EXTRA="-v ${PWD}/tests:/tests"  \
	$(MAKE) run
